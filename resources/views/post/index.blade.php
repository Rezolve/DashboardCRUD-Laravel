@extends('layouts.main')

@section('title')
    Notes of a SysAdmin
@endsection

@section('content')
    <main role="main" class="container"  style="margin-top: 5px">

        <div class="row">

            <div class="col-sm-8 blog-main">

                <div class="blog-post">
                    <h2 class="blog-post-title">Posts One</h2>
                    <p class="blog-post-meta"><small><i>Mah 12, 2018 by <a href="#">Adrian</a></i></small></p>

                    <p>Cum sociis natoque penatibus et magnis <a href="#">dis parturient montes</a>, nascetur ridiculus mus. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Sed posuere consectetur est at lobortis. Cras mattis consectetur purus sit amet fermentum.</p>
                    <blockquote>
                        <p>Curabitur blandit tempus porttitor. <strong>Nullam quis risus eget urna mollis</strong>
                            ornare vel eu leo. Nullam id dolor id nibh ultricies vehicula
                            ut id elit... <a href="" class="btn btn-dark btn-sm">Learn more</a> </p>
                    </blockquote>
                </div><!-- /.blog-post -->

                <div class="blog-post">
                    <h2 class="blog-post-title">Post Two</h2>
                    <p class="blog-post-meta"><small><i>May 11, 2018 by <a href="#">Admin</a></i></small></p>

                    <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                        Aenean lacinia bibendum nulla sed consectetur. Etiam porta
                        euismod. Fusce dapibus... <a href="" class="btn btn-dark btn-sm">Learn more</a> </p>
                </div><!-- /.blog-post -->

                <nav class="blog-pagination">
                    <a class="btn btn-outline-primary" href="#">Older</a>
                    <a class="btn btn-outline-secondary disabled" href="#">Newer</a>
                </nav>

            </div><!-- /.blog-main -->

            <aside class="col-sm-3 ml-sm-auto blog-sidebar">
                <div class="sidebar-module">
                    <h4>Latest Posts</h4>
                    <ol class="list-unstyled">
                        <li><a href="#">April 2018</a></li>
                        <li><a href="#">March 2018</a></li>
                        <li><a href="#">February 2018</a></li>
                        <li><a href="#">January 2018</a></li>
                        <li><a href="#">December 2017</a></li>
                        <li><a href="#">November 2017</a></li>
                        <li><a href="#">October 2017</a></li>
                        <li><a href="#">September 2017</a></li>
                        <li><a href="#">August 2017</a></li>
                        <li><a href="#">July 2017</a></li>
                        <li><a href="#">June 2017</a></li>
                        <li><a href="#">May 2017</a></li>
                        <li><a href="#">April 2017</a></li>
                    </ol>
                </div>
                <div class="sidebar-module">
                    <h4>Elsewhere</h4>
                    <ol class="list-unstyled">
                        <li><a href="#">GitHub</a></li>
                        <li><a href="#">Twitter</a></li>
                        <li><a href="#">Facebook</a></li>
                    </ol>
                </div>
            </aside><!-- /.blog-sidebar -->

        </div><!-- /.row -->

    </main><!-- /.container -->
@endsection